var mrz;// = localStorage.getItem("mrz");
var parsedMRZ;


if(navigator.onLine){
  readDataOnline();
}
else{
  readDataOffline();
}

//var parsedMRZ = parse(localStorage.getItem("mrz"));
function loadData() {
	document.getElementById("pNo").innerHTML = parsedMRZ.documentNumber;
	document.getElementById("name").innerHTML = parsedMRZ.names.names[0] + " " + parsedMRZ.names.lastName;
	document.getElementById("nationality").innerHTML = parsedMRZ.nationality.full;
	document.getElementById("dob").innerHTML = parsedMRZ.dob.day + "/" + parsedMRZ.dob.month + "/" + parsedMRZ.dob.year;
	document.getElementById("sex").innerHTML = parsedMRZ.sex.full;
	document.getElementById("expiry").innerHTML = parsedMRZ.expiry.day + "/" + parsedMRZ.expiry.month + "/" + parsedMRZ.expiry.year;
	document.getElementById("issuer").innerHTML = parsedMRZ.issuer;
}

function readDataOffline(){
  var urlParams = new URLSearchParams(window.location.search)
  var db = JSON.parse(localStorage.getItem("recentDB"));
  var doc;
  if (urlParams.has('docNum')) {
    for(var key in db) {
      if(db.hasOwnProperty(key)) {
        var val = db[key];
        for(var pp in val){
          if(val.hasOwnProperty(pp)){
            doc = val[pp];
            if(urlParams.get('docNum') === doc.documentNumber){
              parsedMRZ = doc;
              console.log(parsedMRZ);
              loadData();
            }
          }
        }
      }
    }
  }
  else{
    parsedMRZ = parse(localStorage.getItem("mrz"));
    loadData();
  }
}

function readDataOnline(){
  var config = {
    apiKey: "AIzaSyDgwEp9-mqFC4RAhs8oAch16cAj5suVsnY",
    authDomain: "pwa-pp-validation.firebaseapp.com",
    databaseURL: "https://pwa-pp-validation.firebaseio.com",
    projectId: "pwa-pp-validation",
    messagingSenderId: "878844319248"
  };
  firebase.initializeApp(config);
  database = firebase.database();

  var urlParams = new URLSearchParams(window.location.search)
  console.log(urlParams.get('docNum'));
  if (urlParams.has('docNum')) {
    database.ref().once('value', function (snapshot) {
      if (snapshot.exists()) {
        snapshot.forEach(function (data) {
          data.forEach(function (d) {
            console.log(d.val());
            var val = d.val();
            if(urlParams.get('docNum') === val.documentNumber){
              parsedMRZ = val;
              console.log(parsedMRZ);
              loadData();
            }
          });
        });
      }
    });
  }
  else {
    parsedMRZ = parse(localStorage.getItem("mrz"));
    loadData();
  }
}

function goBack() {
	window.location.href = "/"
}