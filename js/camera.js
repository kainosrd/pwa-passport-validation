var googleApiKey = 'AIzaSyCsq5sTXryB6Oh9r0Yart-3vEER6IFcBdM';

var sessionToken;

var width = 1280;
var height = 0;

var streaming = false;

var video = null;
var canvas = null;
var photo = null;
var startbutton = null;
var localStream;
var savedImg;
var responseJSON;
var mrzLine1;
var mrzLine2;

var config = {
	apiKey: "AIzaSyDgwEp9-mqFC4RAhs8oAch16cAj5suVsnY",
	authDomain: "pwa-pp-validation.firebaseapp.com",
	databaseURL: "https://pwa-pp-validation.firebaseio.com",
	projectId: "pwa-pp-validation",
	messagingSenderId: "878844319248"
};
firebase.initializeApp(config);

var database = firebase.database();
const messaging = firebase.messaging();

messaging.requestPermission()
.then(function(){
  console.log("Permission granted");
  return messaging.getToken();
})
.then(function(token){
  this.sessionToken = token;
})
.catch(function(err){
  console.log("error");
})

messaging.onMessage(function(payload){
  alert(payload.notification.body);
})


function startup() {
	video = document.getElementById('vidStream');
	canvas = document.getElementById('canvas');
	photo = startbutton = document.getElementById('tPicture');

	navigator.getMedia = (navigator.getUserMedia ||
		navigator.webkitGetUserMedia ||
		navigator.mozGetUserMedia ||
		navigator.msGetUserMedia);

	navigator.getMedia(
		{ video: { facingMode: "environment" } },
		function (stream) {
			if (navigator.mozGetUserMedia) {
				video.mozSrcObject = stream;
			} else {
				var vendorURL = window.URL || window.webkitURL;
				video.src = vendorURL.createObjectURL(stream);
			}
			video.play();
			localStream = stream.getTracks()[0];
		},
		function (err) {
			console.log("An error occured! " + err);
		}
	);

	video.addEventListener('canplay', function (ev) {
		if (!streaming) {
			height = video.videoHeight / (video.videoWidth / width);

			if (isNaN(height)) {
				height = width / (4 / 3);
			}

			video.setAttribute('width', width);
			video.setAttribute('height', height);
			canvas.setAttribute('width', width);
			canvas.setAttribute('height', height);
			streaming = true;
		}
	}, false);

}

function clearPicture() {
	var context = canvas.getContext('2d');
	context.fillStyle = "#AAA";
	context.fillRect(0, 0, canvas.width, canvas.height);

	var data = canvas.toDataURL('image/png');
	document.getElementById("output").setAttribute('src', data);


	console.log("Picture Cleared");
	document.getElementById("output").outerHTML = '<video autoplay id="vidStream" style="width:inherit; background-color:black; border-radius:10px;"></video>';
	document.getElementById("clearPicture").outerHTML = '<a class="waves-effect waves-light btn red" id="tPicture" onclick="tPicture()">Take picture</a>';
	document.getElementById("submit").outerHTML = '';
	savedImg = null;
	restartStream();
}

function tPicture() {
	var context = canvas.getContext('2d');
	if (width && height) {
		canvas.width = width;
		canvas.height = height;
		context.drawImage(video, 0, 0, width, height);
		document.getElementById("vidStream").outerHTML = '<img id="output" style="width:inherit; background-color:black; border-radius:10px;" alt="Scanned img.">'


		var data = canvas.toDataURL('image/png');
		document.getElementById('output').setAttribute('src', data);
		savedImg = data.toString().substring(22);

		document.getElementById("tPicture").outerHTML = '<a class="waves-effect waves-light btn red" id="clearPicture" onclick="clearPicture()">clear picture</a>';
		document.getElementById("clearPicture").insertAdjacentHTML('afterend', '<a class="waves-effect waves-light btn red" style="margin-left:1%;" id="submit" onclick="submit()">Submit</a>');

		this.localStream.stop();
	} else {
		clearPicture();
	}



}

function restartStream() {
	navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } }).then(function (stream) {
		video = document.querySelector('video');
		video.src = window.URL.createObjectURL(stream);

		video.onloadedmetadata = function (e) {
			// Ready to go. Do some stuff.
		};
		this.localStream = stream.getTracks()[0];
	});
}

function submit() {
	document.getElementById("loadingHidden").outerHTML = '<div id="loadingDiv"><h5 id="loading">Loading</h5></div>'
	if (navigator.onLine) {
		var request = JSON.stringify(
			{
				"requests": [
					{
						"image": {
							"content": savedImg
						},
						"features": [
							{
								"type": "DOCUMENT_TEXT_DETECTION",
							}
						]
					}
				]
			});
		var oReq = new XMLHttpRequest();
		oReq.onreadystatechange = function () {
			if (oReq.readyState === 4) {
				if (oReq.status === 200) {
					var count = 1;
					responseJSON = JSON.parse(oReq.responseText);
					if (responseJSON.responses[0].textAnnotations != undefined) {
						if (responseJSON.responses[0].textAnnotations[0].description.split("\n") != undefined) {
							var response = responseJSON.responses[0].textAnnotations[0].description.split("\n");
							//console.log(response);
							response.forEach(function (element) {
								//var e = element.replace(" ", "");
								var e = element.split(' ').join('');
								console.log(e + " - " + e.length);
								if (e.length === 44 && count === 1) {
									mrzLine1 = e;
									count++;
								} else if (e.length === 44 && count === 2) {
									mrzLine2 = e;
									count++;
								}
								else if (e.length === 44 && count > 2) {
									count++;
								}
								else { }
							})

							if (count != 3) {
								alert("Passport MRZ unreadable, please try again");
								document.getElementById("loadingDiv").outerHTML = '<div id="loadingHidden"><h5 id="loadingHidden">Loading</h5></div>';
								clearPicture();
							}
							else {
								console.log((mrzLine1 + mrzLine2).length)
								localStorage.setItem("mrz", mrzLine1 + mrzLine2);
								database.ref("MRZ list").push(parse(localStorage.getItem("mrz")));
								window.location.href = '/identity.html';
								sendConfirmationPush("Scan has been loaded into database");
							}
						}
					}
					else {
						sendConfirmationPush("No words found, please try again");
						document.getElementById("loadingDiv").outerHTML = '<div id="loadingHidden"><h5 id="loadingHidden">Loading</h5></div>';
						clearPicture();
					}
				}
				else if (oReq.status === 401) {
					sendConfirmationPush("Google restricted access to API, please check Google cloud vision API key is correct.");
					document.getElementById("loadingDiv").outerHTML = '<div id="loadingHidden"><h5 id="loadingHidden">Loading</h5></div>';
					clearPicture();	
				}
			}
		}
		oReq.open("POST", "https://vision.googleapis.com/v1/images:annotate?key=AIzaSyCsq5sTXryB6Oh9r0Yart-3vEER6IFcBdM");
		oReq.setRequestHeader("Content-Type", "application/json")
		oReq.send(request);
	}
	else {
		alert("No connection available, photo will be scanned when network is found");
		document.getElementById("loadingDiv").outerHTML = '<div id="loadingHidden"><h5 id="loadingHidden">Loading</h5></div>';
		localStorage.setItem("offlineImage", savedImg);
		window.addEventListener('online', function (e) {
			console.log("fired");
			savedImg = localStorage.getItem("offlineImage");
			localStorage.setItem("offlineImage", '');
			submit();
		}, false);
	}

}

window.addEventListener('load', startup, false);

var hidden, visibilityChange;
if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
	hidden = "hidden";
	visibilityChange = "visibilitychange";
} else if (typeof document.msHidden !== "undefined") {
	hidden = "msHidden";
	visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
	hidden = "webkitHidden";
	visibilityChange = "webkitvisibilitychange";
}

// If the page is hidden, pause the video;
// if the page is shown, play the video
function handleVisibilityChange() {
	if (document[hidden]) {
		console.log("HIDDEN");
		localStream.stop();
	} else {
		if (document.getElementById("output")) {
			console.log("OUTPUT ACTIVE")
		}
		else {
			console.log("ACTIVE");
			restartStream();
		}
	}
}

// Warn if the browser doesn't support addEventListener or the Page Visibility API
if (typeof document.addEventListener === "undefined" || typeof document[hidden] === "undefined") {
	console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
} else {
	// Handle page visibility change   
	document.addEventListener(visibilityChange, handleVisibilityChange, false);
}



function sendConfirmationPush(confString){
  $.ajax({
    type : 'POST',
    url : "https://fcm.googleapis.com/fcm/send",
    headers : {
        Authorization : 'key=' + 'AAAAzJ80FhA:APA91bHbeB-QUGEsNalbeZIov5lXM4m-o5cJ-RuPjbL5LySFE545IF439ZWepLKeUmRanzHO-CmyRX1tTuf013CLhHnRkVtNn-wMOhi_vdKY5eL-ZmhqSqtJf1EhJ-9B47R76u5gWxyX'
    },
    contentType : 'application/json',
    data : JSON.stringify({
      "to": sessionToken,
      "notification": {
        "title": "Data sent",
				"body": "Your Scan has been analysed. \n" + confString,
       }}),

    success : function(response) {
    },
    error : function(xhr, status, error) {
    }
}); 
}