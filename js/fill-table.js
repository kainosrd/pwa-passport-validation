

if(navigator.onLine){
	var database = firebase.database();

	database.ref().once('value', function (snapshot) {
		if (snapshot.exists()) {
      readData(snapshot.val());
      localStorage.setItem("recentDB", JSON.stringify(snapshot));           
		}
	});
}

else{
	if(localStorage.getItem("recentDB") != null){
    console.log(JSON.parse(localStorage.getItem("recentDB")));
		readData(JSON.parse(localStorage.getItem("recentDB")));
	}
	else{
		alert("No offline database file found")
	}
}

function readData(data){
  var db = data;
  var doc;
  var content = '';

    for(var key in db) {
      if(db.hasOwnProperty(key)) {
        var val = db[key];
        for(var pp in val){
          if(val.hasOwnProperty(pp)){
            doc = val[pp];
            content += '<tr onclick="rowClick(\'' + doc.documentNumber + '\')">';
            content += '<td>' + doc.documentNumber + '</td>';
            content += '<td>' + doc.names.names[0] + " " + doc.names.lastName + '</td>';
            content += '<td>' + doc.nationality.full + '</td>';
            content += '</tr>';
          }
        }
      }
    }
  document.getElementById('scan-table').insertAdjacentHTML("BeforeEnd", content);
}

