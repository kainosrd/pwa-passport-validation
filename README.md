# README #

Readme on the passport application about PWAs. Application will call to google cloud vision API to scan passport MRZs.
The service worker will register on first startup and load in the most recent database on firebase to use. This means it can show previous passport listings while offline and interact with them in detail.


### How do I get set up? ###

It's pretty easy to get going. Download/clone the repo then navigate to the directory in the terminal and execute the following:

$ python -m SimpleHTTPServer <port number>

after this has run, head to your localhost at that port and you'll have a nice PWA passport app there for you.

### Who do I talk to? ###

Dev: Adam Grimley

Upkeep: Adam Grimley

###KNOWN BUGS###

- It has been noted to break when using url parameters when offline.