//update version number to trigger sw update
var cacheName = '21';
//files to cache - add to list new files
var cacheFiles = [
    './',
    'index.html',
    'identity.html',
    'scan-list.html',
    'js/MRZ-parser/index.js',
    'js/camera.js',
    'js/fill-table.js',
    'js/identityLoad.js',
    'js/listing.js',
    'css/materialize.css',
    'js/materialize.js'
];

//install method - first called - when new sw is downloaded
self.addEventListener('install', function (event) {
    console.log('Installing service worker');
    event.waitUntil(
        caches.open(cacheName).then(function (cache) {
            return cache.addAll(cacheFiles);
        })
    );
});

//activate method - called when sw becomes active - use to delete old cache files
self.addEventListener('activate', function (e) {
    console.log('Service worker activated');

    e.waitUntil(
        caches.keys().then(function (cacheNames) {
            return Promise.all(cacheNames.map(function (thisCacheName) {
                if (thisCacheName !== cacheName) {
                    console.log('Removing cached files from ', thisCacheName);
                    return caches.delete(thisCacheName);
                }
            }))
        })
    )
});

//fetch method - checks if new files match old files - decides what to return
self.addEventListener('fetch', function (event) {
    console.log('Fetching service worker');
    event.respondWith(
        caches.match(event.request).then(function (response) {
            return response || fetch(event.request);
        })
    );
});

